add_library(kinematics
    robotArmExample.cpp
)

target_include_directories(kinematics PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(kinematics
    linear_algebra
)
