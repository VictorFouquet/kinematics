#include "robotArmExample.h"

int main()
{
    RobotArm::forwardKinematic2DOF_DEMO();
    RobotArm::forwardKinematics6DOF(180.f, 45.f, -90.f, 45.f, 90.f, 30.f);

    return 0;
}